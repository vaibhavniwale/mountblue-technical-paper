//Function for creating Developers in which we have all the developer's feature
function Developer(name)
{
  this.name = name
  this.type = "Developer"
}
//Function for creating testers in which we have all the tester's feature
function Tester(name)
{
  this.name = name
  this.type = "Tester"
}
//function for creating employees
function EmployeeFactory()
{
  this.create = (name, type) => {
    switch(type)
    {
      case 1:
        return new Developer(name)
      case 2:
        return new Tester(name)
    }
  }
}
//for print out there details
function say()
{
  console.log("Hi, I am " + this.name + " and I am a " + this.type)
}

const employeeFactory = new EmployeeFactory()
// for now let it be database
const employees = []
//adding employees
employees.push(employeeFactory.create("Patrick", 1))
employees.push(employeeFactory.create("John", 2))
employees.push(employeeFactory.create("Jamie", 1))
employees.push(employeeFactory.create("Taylor", 1))
employees.push(employeeFactory.create("Tim", 2))
//printing there details
employees.forEach( emp => {
  say.call(emp)
})
// output
/*
Hi, I am Patrick and I am a Developer
Hi, I am John and I am a Tester
Hi, I am Jamie and I am a Developer
Hi, I am Taylor and I am a Developer
Hi, I am Tim and I am a Tester
*/