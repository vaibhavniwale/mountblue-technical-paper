# JavaScript Design Patterns
## What is a Design Pattern?
 >In software engineering, a design pattern is a general repeatable solution to a commonly occurring problem in software design. A design pattern isn't a finished design that can be transformed directly into code. It is a description or template for how to solve a problem that can be used in many different situations.

## Basic Groups of Design Patterns
> All the design patterns are mainly grouped into 3 design patterns
> 1. Creational Pattern 
> 2. Structural Pattern 
> 3. Behavioral Pattern
---

## What is Creational Pattern?
> Creational design patterns focus on handling object-creation mechanisms where objects are created in a manner suitable for a given situation. The basic approach to object creation might otherwise lead to added complexity in a project, while these patterns aim to solve this problem by controlling the creation process.

>There are many creational design patterns but we are discussing only two patterns. 
> 1. Factory Pattern
> 2. Singleton Pattern
---
### Factory Pattern
> Define an interface for creating an object, but let subclasses decide which class to instantiate. Factory Method lets a class defer instantiation to subclasses.

For example, lets consider we have a software company. we have two types of employees.
1. Developers - write code for new features.
2. Testers - write the tests for new features.

we want to set up a database so we can keep track of all the employees. so we want an easy way to create multiple employees and adding into a database(for now just keep it a simple array). 
```JavaScript
// for now let it be database
const employees = []
```
So we create a 2 functions developer and tester in which we have their features(for now just consider name and title as features). 
```JavaScript
//Function for creating Developers in which we have all the developer's feature
function Developer(name)
{
  this.name = name
  this.type = "Developer"
}
//Function for creating testers in which we have all the tester's feature
function Tester(name)
{
  this.name = name
  this.type = "Tester"
}
```
Now we create an employeeFactory function in which we call the developer and tester function based on parameters passed. 
```JavaScript
//function for creating employees
function EmployeeFactory()
{
  this.create = (name, type) => {
    switch(type)
    {
      case 1:
        return new Developer(name)
      case 2:
        return new Tester(name)
    }
  }
}

const employeeFactory = new EmployeeFactory()
```
so now using the employeeFactory function we can create multiple employees.
```JavaScript   
//for print out there details
function say()
{
  console.log("Hi, I am " + this.name + " and I am a " + this.type)
}

//adding employees
employees.push(employeeFactory.create("Patrick", 1))
employees.push(employeeFactory.create("John", 2))
employees.push(employeeFactory.create("Jamie", 1))
employees.push(employeeFactory.create("Taylor", 1))
employees.push(employeeFactory.create("Tim", 2))
//printing there details
employees.forEach( emp => {
  say.call(emp)
})
// output
/*
Hi, I am Patrick and I am a Developer
Hi, I am John and I am a Tester
Hi, I am Jamie and I am a Developer
Hi, I am Taylor and I am a Developer
Hi, I am Tim and I am a Tester
*/
```
### Singleton Pattern
>The Singleton design pattern is a creational pattern that states that one and only one instance of a class would persist in the memory during the application's life cycle. In other words, this design pattern restricts the instantiation of a class to just one object.

For example, Let's consider we want to write a program that manages the process and the 2 main components are process and process manager. so we can have one or more processes but we want to have one process manager to manage those processes. So we create a variable and set it equal to an invoked function expression. In that function expression, we will put any object constructor for any objects that we want to limit so we create the function ProjectManager. The next thing we need to do is store a reference to ProjectManager so we create createProcessManager. This function is not exposed to outside so we can limit the ProjectManager. Now we need to call the ProcessManager form outside but only once to do that we create getProjectManger and checks if the pManager variable is empty or has values if it's empty we call createProcessManager and assign ProcessManager to pManager so next time the getProcessManager is called it will return the earlier value.
```JavaScript
// createing a variable 
const Singleton = (function(){
    let pManager
    // ProcessManager
    function ProcessManager() {
        this.numProcess = 0
    }
    // Create a instance of ProcessManager
    function createProcessManager()
    {
      pManager = new ProcessManager()
      return pManager
    }
    
    return {
        getProcessManager: () =>
        {
          //checking is ProcessManager instance is already present
          if(!pManager)
            pManager = createProcessManager()
          return pManager
        }
    }
  })()
  
  const singleton = Singleton.getProcessManager()
  const singleton2 = Singleton.getProcessManager()
  //checking if both instances are same
  console.log(singleton/* === singleton2*/) // true
```
---
## What is Structural Pattern?
> Structural patterns are concerned with object composition and typically identify simple ways to realize relationships between different objects. They help ensure that when one part of a system changes, the entire structure of the system doesn't need to do the same.

>There are many structural design patterns but we are discussing only one pattern 
> 1. Proxy Pattern
---
### Proxy Pattern
> In the proxy pattern, one object acts as an interface to another object. The proxy sits between the client of an object and the object itself and protects access to that object.

For example, Lets us consider we are building a program that displays values for a different cryptocurrency. But to get these values we need to request some external API. For that let us create a function CryptocurrencyAPI which makes the request and gives the value back. 
```JavaScript
// consider this as external API services
function CryptocurrencyAPI()
{
  this.getValue = function(coin)
  {
    console.log("Calling External API...")
    switch(coin)
    {
      case "Bitcoin":
        return "$8,500"
      case "Litecoin":
        return "$50"
      case "Ethereum":
        return "$175"
       default:
        return "NA"
    }
  }
}
//getting the values without proxy
const api = new CryptocurrencyAPI()
console.log("----------Without Proxy----------")
console.log(api.getValue("Bitcoin"))
console.log(api.getValue("Litecoin"))
console.log(api.getValue("Ethereum"))
console.log(api.getValue("Bitcoin"))
console.log(api.getValue("Litecoin"))
console.log(api.getValue("Ethereum"))
// Output
/*
----------Without Proxy----------
Calling External API...
$8,500
Calling External API...
$50
Calling External API...
$175
Calling External API...
$8,500
Calling External API...
$50
Calling External API...
$175
*/
```
let consider that we have to make a lot of requests for a few requests it does not matter but if we have to make like 50 requests and these are network requests so it is going to take some time to retrieve the data. So that's not exactly ideal. SO we can have an object or proxy in the middle of the process that talks to the API for us. Now we can add a cache. we call our proxy it check if it is in the cache then it returns the same value but if it does not have it then we call the API.
```JavaScript
// Creating a proxy with cache
function CryptocurrencyProxy()
{
  this.api = new CryptocurrencyAPI()
  this.cache = {}

  this.getValue = function(coin)
  {
    if(this.cache[coin] == null)
    {
      this.cache[coin] = this.api.getValue(coin)
    }
    return this.cache[coin]
  }
}

console.log("----------With Proxy----------")
const proxy = new CryptocurrencyProxy()
console.log(proxy.getValue("Bitcoin"))
console.log(proxy.getValue("Litecoin"))
console.log(proxy.getValue("Ethereum"))
console.log(proxy.getValue("Bitcoin"))
console.log(proxy.getValue("Litecoin"))
console.log(proxy.getValue("Ethereum"))
// Output
/*
----------With Proxy----------
Calling External API...
$8,500
Calling External API...
$50
Calling External API...
$175
$8,500
$50
$175
*/
```
---
## What is Behavioral Pattern?
>Behavioral patterns focus on improving or streamlining the communication between disparate objects in a system.

>There are many behavioral design patterns but we are discussing only two patterns 
> 1. Iterator Pattern
> 2. Strategy Pattern
---
### Iterator Pattern
>The Iterator is a design pattern in which iterators (objects that allow us to traverse through all the elements of a collection) access the elements of an aggregate object sequentially without needing to expose its underlying form.

For example, In this example, we are going to create an iterator that traverses over an array. So we create an array or collection and function iterator(which sets the index to 0). 
```JavaScript
// array of mixed types
const items = [1, false, "vsvn", 3.14]
// Iterator iterates from front to back
function Iterator(items)
{
  this.items = items
  this.index = 0
}
```
Now we need methods for our iterator. Iterators must have 2 critical method 
1. hasNext - returns true if there is any next item else false
2. next - returns element on the present index and add 1 to index so next time we call it will give us the next value.
```JavaScript
Iterator.prototype = {
  hasNext: function()
  {//hasNext - returns true if there is any next item else false
    return this.index < this.items.length
  },
  next: function()
  {//next - returns element in our collection
    return this.items[this.index++]
  }
}

const iter = new Iterator(items)

console.log(iter.hasNext())
//Itorator working
while(iter.hasNext()){
  console.log(iter.next())
  console.log(iter.hasNext())
}
//Output
/*
true
1
true
false
true
vsvn
true
3.14
false
*/
```
### Strategy Pattern
>The Strategy pattern encapsulates alternative algorithms (or strategies) for a particular task. It allows a method to be swapped out at runtime by any other method (strategy) without the client realizing it. Essentially, Strategy is a group of interchangeable algorithms.

For example, let us write a program that gives different shipping companies. For that, we need to have a different object for each shipping company that has a calculation method according to the company for now just return some values. 
```JavaScript
// creating objects of shipping companies
function Fedex(pkg)
{
  this.calculate = () =>
  {
    // Fedex calculations ...
    return 2.45
  }
}

function UPS(pkg)
{
  this.calculate = () =>
  {
    // UPS calculations ...
    return 1.56
  }
}

function USPS(pkg)
{
  this.calculate = () =>
  {
    // USPS calculations ...
    return 4.5
  }
}
```
Now we have to encapsulate all the above functions into one place so we create an object constructor Shipping. In Shipping, we have two methods
1. setStrategy - Takes company name and assign to this.company
2. calculate - takes the package and returns the calculation of this.company 
```JavaScript
// Encapsulating above objects
function Shipping()
{
  this.company = ''
  this.setStrategy = company =>{
    this.company = company
  }
  this.calculate = pkg => {
    return this.company.calculate(pkg)
  }
}

const fedex = new Fedex()
const ups = new UPS()
const usps = new USPS()
const shipping = new Shipping()
const pkg = { from: "Alabama", to: "Georgia", weight: 1.56 } // Dummy package

shipping.setStrategy(fedex)
console.log("Fedex: " + shipping.calculate(pkg))

shipping.setStrategy(ups)
console.log("UPS: " + shipping.calculate(pkg))

shipping.setStrategy(usps)
console.log("USPS: " + shipping.calculate(pkg))
// Output
/*
Fedex: 2.45
UPS: 1.56
USPS: 4.5
*/
```