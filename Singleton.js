// createing a variable 
const Singleton = (function(){
    let pManager
    // ProcessManager
    function ProcessManager() {
        this.numProcess = 0
    }
    // Create a instance of ProcessManager
    function createProcessManager()
    {
      pManager = new ProcessManager()
      return pManager
    }
    
    return {
        getProcessManager: () =>
        {
          //checking is ProcessManager instance is already present
          if(!pManager)
            pManager = createProcessManager()
          return pManager
        }
    }
  })()
  
  const singleton = Singleton.getProcessManager()
  const singleton2 = Singleton.getProcessManager()
  //checking if both instances are same
  console.log(singleton/* === singleton2*/) // true