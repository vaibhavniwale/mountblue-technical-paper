// array of mixed types
const items = [1, false, "vsvn", 3.14]
// Iterator iterates from front to back
function Iterator(items)
{
  this.items = items
  this.index = 0
}

Iterator.prototype = {
  hasNext: function()
  {//hasNext - returns true if there is any next item else false
    return this.index < this.items.length
  },
  next: function()
  {//next - returns element in our collection
    return this.items[this.index++]
  }
}

const iter = new Iterator(items)

console.log(iter.hasNext())
//Itorator working
while(iter.hasNext()){
  console.log(iter.next())
  console.log(iter.hasNext())
}
//Output
/*
true
1
true
false
true
vsvn
true
3.14
false
*/